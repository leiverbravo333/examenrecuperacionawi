var datos = function(){
    var codigo = document.getElementById("codigo");
    var titulo = document.getElementById("titulo");
    var autor = document.getElementById("autor");
    var editorial = document.getElementById("editorial");
    var año_registro = document.getElementById("año-registro");
    var año_publicacion = document.getElementById("año-publicacion");

    if(codigo.value === "" || titulo.value === "" || autor.value === "" || editorial === "" || año_registro === "" || año_publicacion === ""){
        alert('Complete todos los campos!');
    }else {
        const ingreso = pool.query("INSERT INTO Libros (codigo, titulo, autor, editorial, año_registro, año_publicacion) values($1, $2, $3, $4, $5, $6)", 
        [
            codigo.value,
            titulo.value,
            autor.value,
            editorial.value,
            año_registro.value,
            año_publicacion.value
        ])
    } 
}