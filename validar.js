document.addEventListener("DOMContentLoaded", function () {
  document
    .getElementById("formulario")
    .addEventListener("submit", validarFormulario);
});

function validarFormulario(evento) {
  evento.preventDefault();
  var codigo = document.getElementById("codigo").value;
  var alfanumerico = /^[0-9a-zA-z]{1}+&/;
  if (codigo.length != 5) {
    alert("El código debe tener 5 caracteres");
    return;
  } else {
    if (!codigo.match(alfanumerico)) {
      return alert("Introducir al menos una letra y un número");
    }
  }
  var titulo = document.getElementById("titulo").value;
  if (titulo.length != 100) {
    alert("El titulo debe tener 100 caracteres");
    return;
  } else {
    if (!titulo.match(alfanumerico)) {
      return alert("Introducir al menos una letra y un número");
    }
  }

  var autor = document.getElementById("autor").value;
  if (autor.length != 60) {
    alert("El codigo debe tener 60 caracteres");
    return;
  } else {
    if (!autor.match(alfanumerico)) {
      return alert("Introducir al menos una letra y un número");
    }
  }

  var editorial = document.getElementById("editorial").value;
  if (editorial.length != 30) {
    alert("El codigo debe tener 5 caracteres");
    return;
  } else {
    if (!editorial.match(alfanumerico)) {
      return alert("Introducir al menos una letra y un número");
    }
  }

  var tipofecha = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;

  var año_publicacion = document.getElementById("año_publicacion").value;

  if (año_publicacion.match(tipofecha) && año_publicacion != "") {
    return true;
  } else {
    alert("Debe ingresar una fecha");
  }

  var año_registro = document.getElementById("año_registro").value;

  if (año_registro.match(tipofecha) && año_registro != "") {
    return true;
  } else {
    alert("Debe ingresar una fecha");
  }

  if (Date.parse(año_registro) < Date.parse(año_publicacion)) {
    alert("Fecha de registro debe ser mayor a fecha de publicacion");
    return;
  }

  this.submit();
}
